import { RefObject, useLayoutEffect } from 'react';
export declare const useOnEscape: (handler: () => void, active?: boolean) => void;
export declare const useRepositionOnResize: (handler: () => void, active?: boolean) => void;
export declare const useOnClickOutside: (ref: RefObject<HTMLElement> | RefObject<HTMLElement>[], handler: () => void, active?: boolean) => void;
export declare const useTabbing: (contentRef: RefObject<HTMLElement>, active?: boolean) => void;
export declare const useIsomorphicLayoutEffect: typeof useLayoutEffect;
