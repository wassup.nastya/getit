export enum Language {
  C = 0,
  JavaScript,
  Java,
  Python,
  Kotlin,
  Dart,
}

export enum Technology {
  AI = 0,
  IoT,
  Backend,
  Frontend,
  ML,
}

export const mapLanguage = new Map<number, string>([
  [0, 'C'],
  [1, 'JavaScript'],
  [2, 'Java'],
  [3, 'Python'],
  [4, 'Kotlin'],
  [5, 'Dart'],
]);

export const mapTechnology = new Map<number, string>([
  [0, 'AI'],
  [1, 'IoT'],
  [2, 'Backend'],
  [3, 'Frontend'],
  [4, 'ML'],
]);
