import firebase from '../firebase';
import { getQuery, sortByUsers } from './helper';
import { Auth, Event, GetEventsRequest, User } from './model';

export const getEvent = (id: string) => {
  return firebase.db
    .collection('event')
    .doc(id)
    .get()
    .then((data) => {
      const event = data.data() as Event;
      return {
        ...event,
        id,
        date: new firebase.firebase.firestore.Timestamp(
          data.data()?.date.seconds,
          data.data()?.date.nanoseconds
        ).toDate(),
        creator: (event as any).creator?.id ?? '',
      } as Event;
    });
};

export const auth = (data: {
  info: Auth;
  onResponse: (response: any) => void;
}) => {
  return firebase.firebase
    .auth()
    .signInWithEmailAndPassword(data.info.login, data.info.password)
    .then((data) => data)
    .catch((error) => error);
};

export const logout = () => {
  return firebase.firebase.auth().signOut();
};

export const addEvent = (data: {
  event: Event;
  onResponse: (response: string) => void;
}) => {
  return firebase.db
    .collection('event')
    .add({
      ...data.event,
      creator: firebase.db.collection('user').doc(data.event.creator),
    })
    .then((x) => x.id);
};

export const getEvents = (data: GetEventsRequest) => {
  return getQuery(data, firebase.db.collection('event'))
    .get()
    .then((response: any) => {
      let list: Event[] = [];
      response.forEach((x: any) => {
        const event = x.data() as Event;
        list.push({
          ...event,
          id: x.id,
          creator: (event as any).creator?.id ?? '',
          date: new firebase.firebase.firestore.Timestamp(
            x.data()?.date.seconds,
            x.data()?.date.nanoseconds
          ).toDate(),
        });
      });
      if (data.orderByField === 'users')
        list = sortByUsers(list, data.orderBy === 'asc');
      return list;
    });
};

export const getUser = (id: string) => {
  return firebase.db
    .collection('user')
    .doc(id)
    .get()
    .then((data) => {
      const events: string[] = [];
      data?.data()?.events.forEach((y: any) => events.push(y.id));
      const user: User = {
        id,
        events,
      };
      return user;
    });
};

export const updateUser = (data: User) => {
  return firebase.db
    .collection('user')
    .doc(data.id)
    .set({
      events: data.events.map((x) => firebase.db.collection('event').doc(x)),
    })
    .then(() => console.log('Success!'))
    .catch((error) => console.log(error));
};

export const updateEvent = (data: Event) => {
  return firebase.db
    .collection('event')
    .doc(data.id)
    .set({
      users: data.users,
      title: data.title,
      description: data.description,
      date: data.date,
      tags: data.tags,
      creator: firebase.db.collection('user').doc(data.creator),
    })
    .catch((error) => console.log(error));
};
