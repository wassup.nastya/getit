import { GetEventsRequest, Event } from './model';

export const getQuery = (filter: GetEventsRequest, query: any) => {
  const filterQuery: any[] = [];
  const sortQuery: any[] = [];

  if (filter.tags.length > 0)
    filterQuery.push({
      field: 'tags',
      operator: 'array-contains-any',
      value: filter.tags,
    });

  if (filter.fromDate != null)
    filterQuery.push({ field: 'date', operator: '>=', value: filter.fromDate });

  if (filter.toDate != null)
    filterQuery.push({ field: 'date', operator: '<=', value: filter.toDate });

  let result = filterQuery.reduce((accQuery, filter) => {
    return accQuery.where(filter.field, filter.operator, filter.value);
  }, query);

  if (filter.orderByField === 'date')
    sortQuery.push({ field: 'date', order: filter.orderBy });

  result = sortQuery.reduce((accQuery, sort) => {
    return accQuery.orderBy(sort.field, sort.order);
  }, result);

  return result;
};

export const sortByUsers = (list: Event[], asc?: boolean) =>
  list.sort((a, b) => {
    const aLength = a.users.length;
    const bLength = b.users.length;
    return asc ? aLength - bLength : bLength - aLength;
  });
