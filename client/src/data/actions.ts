import { ActionType } from './actionType';
import { Action, Auth, Event, GetEventsRequest, User } from './model';

export const getEventAsync: (data: string) => Action<string> = (data) => {
  return { type: ActionType.GETEVENTASYNC, data };
};

export const setEvent: (data: Event) => Action<Event> = (data) => {
  return { type: ActionType.SETEVENT, data };
};

export const authAsync: (data: {
  info: Auth;
  onResponse: (response: firebase.auth.UserCredential) => void;
}) => Action<{ info: Auth; onResponse: (response: any) => void }> = (data) => {
  return { type: ActionType.AUTHASYNC, data };
};

export const logoutAsync: () => Action<any> = () => {
  return { type: ActionType.LOGOUTASYNC };
};

export const setToken: (data: string) => Action<string> = (data) => {
  return { type: ActionType.SETTOKEN, data };
};

export const addEventAsync: (data: {
  event: Event;
  onResponse: (response: string) => void;
}) => Action<{
  event: Event;
  onResponse: (response: string) => void;
}> = (data) => {
  return { type: ActionType.ADDEVENTASYNC, data };
};

export const getEventsAsync: (
  data: GetEventsRequest
) => Action<GetEventsRequest> = (data) => {
  return { type: ActionType.GETEVENTSASYNC, data };
};

export const setEvents: (data: Event[]) => Action<Event[]> = (data) => {
  return { type: ActionType.SETEVENTS, data };
};

export const getUserAsync: (data: string) => Action<string> = (data) => {
  return { type: ActionType.GETUSERASYNC, data };
};

export const updateUserAsync: (data: User) => Action<User> = (data) => {
  return { type: ActionType.UPDATEUSERASYNC, data };
};

export const setUser: (data: User) => Action<User> = (data) => {
  return { type: ActionType.SETUSER, data };
};

export const updateEventAsync: (data: Event) => Action<Event> = (data) => {
  return { type: ActionType.UPDATEEVENTASYNC, data };
};

export const setFilter: (data: string[]) => Action<string[]> = (data) => {
  return { type: ActionType.SETFILTER, data };
};

export const setIsLogin: (data: boolean) => Action<boolean> = (data) => {
  return { type: ActionType.SETISLOGIN, data };
};
