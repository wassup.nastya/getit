import { ActionType } from './actionType';
import { Action, Auth, User, Event } from './model';

export interface StateType {
  event?: Event;
  authInfo: Auth;
  token: string;
  events: Event[];
  user?: User;
  filter: string[];
  isLogin: boolean;
}

const InitialState: StateType = {
  authInfo: { login: '', password: '' },
  token: '',
  events: [],
  filter: [],
  isLogin: false,
};

export const reducer = (state = InitialState, action: Action<any>) => {
  switch (action.type) {
    case ActionType.SETEVENT: {
      return { ...state, event: action.data };
    }
    case ActionType.SETTOKEN: {
      return { ...state, token: action.data };
    }
    case ActionType.SETEVENTS: {
      return { ...state, events: action.data };
    }
    case ActionType.SETUSER: {
      return { ...state, user: action.data };
    }
    case ActionType.SETFILTER: {
      return { ...state, filter: action.data };
    }
    case ActionType.SETISLOGIN: {
      return { ...state, isLogin: action.data };
    }
    default:
      return state;
  }
};
