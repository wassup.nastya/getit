import { ActionsObservable, combineEpics } from 'redux-observable';
import { from } from 'rxjs';
import { ActionType } from './actionType';
import {
  addEvent,
  auth,
  getEvents,
  getUser,
  updateUser,
  getEvent,
  updateEvent,
  logout,
} from './api';
import { filter, ignoreElements, map, mergeMap, tap } from 'rxjs/operators';
import { setEvent, setEvents, setUser } from './actions';
import { isOfType } from 'typesafe-actions';
import { Auth, Event, GetEventsRequest, User } from './model';

const getEventEpic = (
  action$: ActionsObservable<{ type: string; data: string }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.GETEVENTASYNC)),
    mergeMap((data) =>
      from(getEvent(data.data)).pipe(
        map((response: Event) => setEvent(response))
      )
    )
  );

const authEpic = (
  action$: ActionsObservable<{
    type: string;
    data: { info: Auth; onResponse: (response: any) => void };
  }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.AUTHASYNC)),
    mergeMap((data) =>
      from(auth(data.data)).pipe(
        tap((response) => data.data.onResponse(response)),
        ignoreElements()
      )
    )
  );

const logoutEpic = (action$: ActionsObservable<{ type: string }>) =>
  action$.pipe(
    filter(isOfType(ActionType.LOGOUTASYNC)),
    mergeMap(() => from(logout()).pipe(ignoreElements()))
  );

const addEventEpic = (
  action$: ActionsObservable<{
    type: string;
    data: { event: Event; onResponse: (response: string) => void };
  }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.ADDEVENTASYNC)),
    mergeMap((data) =>
      from(addEvent(data.data)).pipe(
        tap((response) => data.data.onResponse(response)),
        ignoreElements()
      )
    )
  );

const getEventsEpic = (
  action$: ActionsObservable<{ type: string; data: GetEventsRequest }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.GETEVENTSASYNC)),
    mergeMap((data) =>
      from(getEvents(data.data)).pipe(
        map((response) => setEvents(response as any))
      )
    )
  );

const getUserEpic = (
  action$: ActionsObservable<{ type: string; data: string }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.GETUSERASYNC)),
    mergeMap((data) =>
      from(getUser(data.data)).pipe(map((response: User) => setUser(response)))
    )
  );

const updateUserEpic = (
  action$: ActionsObservable<{ type: string; data: User }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.UPDATEUSERASYNC)),
    mergeMap((data) => from(updateUser(data.data)).pipe(ignoreElements()))
  );

const updateEventEpic = (
  action$: ActionsObservable<{ type: string; data: Event }>
) =>
  action$.pipe(
    filter(isOfType(ActionType.UPDATEEVENTASYNC)),
    mergeMap((data) => from(updateEvent(data.data)).pipe(ignoreElements()))
  );

export const epic = combineEpics(
  getEventsEpic,
  authEpic,
  addEventEpic,
  getUserEpic,
  updateUserEpic,
  getEventEpic,
  updateEventEpic,
  logoutEpic
);
