export interface Action<T> {
  type: string;
  data?: T;
}

export interface Event {
  date: Date;
  tags: string[];
  creator: string;
  id: string;
  title: string;
  description: string;
  users: string[];
}

export interface User {
  events: string[];
  id: string;
}

export interface Auth {
  login: string;
  password: string;
}

export interface GetEventsRequest {
  orderByField: string;
  orderBy: string;
  tags: string[];
  fromDate?: Date;
  toDate?: Date;
}
