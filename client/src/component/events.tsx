import React, { useCallback, useEffect, useMemo } from 'react';
import { useState } from 'react';
import { Row, Col, Button, FormControl as FormContr } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

import { StoreType } from '../core/rootReducer';
import Popup from 'reactjs-popup';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import { Event } from '../data/model';
import icon from '../images/calendar.png';
import sortUp from '../images/sort-up.svg';
import {
  getEventsAsync,
  logoutAsync,
  setFilter,
  setIsLogin,
  setToken,
} from '../data/actions';
import { Link, useHistory } from 'react-router-dom';
import './events.css';

const allTags = [
  'C',
  'Java',
  'AI',
  'Frontend',
  'Backend',
  'JavaScript',
  'Go',
  'ML',
  'Soft Skills',
  'DevOps',
];

export const Events = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [email, setEmail] = useState('');
  const [popupState, setPopupState] = useState(false);
  const [showRegistration, setShowRegistration] = useState<boolean>(false);
  const [sort, setSort] = useState<{ field: string; type: string }>({
    field: 'date',
    type: 'asc',
  });
  const [localFilter, setLocalFilter] = useState<string[]>([]);

  const { events, filter, user, isLogin } = useSelector(
    (state: StoreType) => state.data
  );

  const sortIconClass = useMemo(() => {
    return sort.type === 'asc' ? 'sort-icon-up' : 'sort-icon-down';
  }, [sort]);

  const getEvents = useCallback(() => {
    dispatch(
      getEventsAsync({
        orderBy: sort.type,
        orderByField: sort.field,
        tags: filter,
      })
    );
  }, [dispatch, sort, filter]);

  useEffect(() => {
    getEvents();
  }, [getEvents]);

  const cancelChanges = useCallback(() => {
    setLocalFilter(filter);
  }, [filter]);

  const copyFilters = useCallback(() => {
    dispatch(setFilter(localFilter));
  }, [dispatch, localFilter]);

  const modalList = useMemo(() => {
    return allTags.map((tag, key) => {
      return (
        <div
          key={key}
          className={localFilter.includes(tag) ? 'active-filter' : 'filter'}
          onClick={() => {
            const tags = localFilter.includes(tag)
              ? localFilter.filter((x) => x !== tag)
              : [...localFilter, tag];
            setLocalFilter(tags);
          }}
        >
          {tag}
        </div>
      );
    });
  }, [localFilter]);

  const filterList = useMemo(() => {
    return filter.map((tag, key) => (
      <div key={key} className="filter-item">
        {tag}
      </div>
    ));
  }, [filter]);

  const handleOpen = () => {
    setPopupState(true);
  };

  const handleClose = () => {
    setPopupState(false);
  };

  const logout = useCallback(() => {
    dispatch(logoutAsync());
    dispatch(setIsLogin(false));
    dispatch(setToken(''));
    history.push('/');
  }, [dispatch, history]);

  const upButtons = isLogin ? (
    <div>
      <div className="log-in">
        <Button size="sm" variant="primary" onClick={logout}>
          Sign out
        </Button>
      </div>
      <div className="log-in add-event">
        <Button
          size="sm"
          variant="primary"
          onClick={() => {
            history.push('/event/new');
          }}
        >
          Add event
        </Button>
      </div>
    </div>
  ) : (
    <div className="log-in">
      <Button
        size="sm"
        variant="primary"
        onClick={() => history.push('/login')}
      >
        Log in
      </Button>
    </div>
  );

  const eventList = events.map((eventItem: Event, key: number) => (
    <div key={key} className="event-item">
      <div className="date-div">
        <p className="date num">{eventItem.date.getDate()}</p>
        <p className="date month">
          {eventItem.date.toLocaleString('en-US', { month: 'short' })}.
        </p>
      </div>
      {isLogin && user?.id === eventItem.creator ? (
        <Link to={`event/${eventItem.id}`}>
          <h1 className="event-title">{eventItem.title}</h1>
        </Link>
      ) : (
        <h1 className="event-title">{eventItem.title}</h1>
      )}
      <p className="event-description">{eventItem.description}</p>
      {!isLogin && (
        <div className="register-btn" onClick={() => setShowRegistration(true)}>
          {user?.events.includes(eventItem.id)
            ? 'You are registered'
            : 'Register'}
        </div>
      )}
      {isLogin ? (<div className="event-line login"></div>) : (<div className="event-line"></div>)}
    
    </div>
  ));

  return (
    <div className="events-container">
      <img src={icon} alt="icon" className="icon-1" />
      {upButtons}
      <Row>
        <Col xs={6} className="upcoming-events">
          Upcoming events
        </Col>
      </Row>
      <div className="sorting">
        <FormControl>
          <InputLabel id="demo-simple-select-label">Sort</InputLabel>
          <Select
            value={sort.field}
            onChange={(event) =>
              setSort({ ...sort, field: event.target.value as string })
            }
            labelId="demo-simple-select-label"
            id="demo-simple-select"
          >
            <MenuItem value="date">Date</MenuItem>
            <MenuItem value="users">Number of people</MenuItem>
          </Select>
        </FormControl>
        <div
          className="arrow"
          onClick={() =>
            setSort({ ...sort, type: sort.type === 'desc' ? 'asc' : 'desc' })
          }
        >
          <img src={sortUp} alt="up" className={sortIconClass} />
        </div>
      </div>
      <div className="filter-items">
        {filterList}
        <div className="add-filter">
          <Popup
            className="filter-popup"
            trigger={
              <button className="btn" onClick={() => {}}>
                +
              </button>
            }
            modal
            on="click"
            open={popupState}
            onOpen={() => {
              handleOpen();
              copyFilters();
            }}
            onClose={() => {
              handleClose();
              cancelChanges();
            }}
          >
            <div className="modal-window">
              <div className="header">
                choose type of event
                <div
                  className="close-btn"
                  onClick={() => {
                    handleClose();
                    cancelChanges();
                  }}
                >
                  <span className="line"></span>
                  <span className="line second"></span>
                </div>
              </div>
              <div className="filters">{modalList}</div>
              <div className="save-filter">
                <button
                  className="button"
                  onClick={() => {
                    handleClose();
                    copyFilters();
                  }}
                >
                  Save
                </button>
              </div>
            </div>
          </Popup>
          <Popup className="filter-popup" modal open={showRegistration}>
            <div className="register-modal">
              <div className="header header-register">
                registration for the event
                <div
                  className="close-btn reg"
                  onClick={() => setShowRegistration(false)}
                >
                  <span className="line"></span>
                  <span className="line second"></span>
                </div>
              </div>
              <div className="register-label">
                Enter your email address to receive an invitation to the event
              </div>
              <FormContr
              className="input-email"
                value={email}
                onChange={(v) => setEmail(v.target.value)}
                size="sm"
              />
                <button
                  className="button"
                  onClick={() => setShowRegistration(false)}
                >
                  Sent
                </button>
            </div>
          </Popup>
        </div>
      </div>
      {eventList}
    </div>
  );
};
