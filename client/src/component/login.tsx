import React, { useCallback, useState } from 'react';
import { FormControl, Row, Col, FormLabel, Button } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { authAsync, getUserAsync, setIsLogin, setToken } from '../data/actions';
import { Auth } from '../data/model';

import './login.css';

export const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [authInfo, setAuthInfo] = useState<Auth>({ login: '', password: '' });
  const [message, setMessage] = useState<string>('');

  const onLogin = useCallback(() => {
    dispatch(
      authAsync({
        info: authInfo,
        onResponse: (response) => {
          if (!response?.user) setMessage(response?.['message']);
          else {
            dispatch(getUserAsync(response.user.uid));
            dispatch(setToken(response.user.uid));
            dispatch(setIsLogin(true));
            history.push('/');
          }
        },
      })
    );
  }, [authInfo, dispatch, history]);

  return (
    <>
      <div className="login-container">
        <FormLabel className="get-it">GET IT</FormLabel>
        <Row className="mt-4">
          <Col xs={6}>
            <FormLabel className="login-label">Login</FormLabel>
            <FormControl
              value={authInfo.login}
              onChange={(v) =>
                setAuthInfo({ ...authInfo, login: v.target.value })
              }
              size="sm"
            />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs={6}>
            <FormLabel className="password-label">Password</FormLabel>
            <FormControl
              value={authInfo.password}
              onChange={(v) =>
                setAuthInfo({ ...authInfo, password: v.target.value })
              }
              type="password"
              size="sm"
            />
          </Col>
          <div>{message !== '' ? message : ''}</div>
        </Row>
        <Row className="mt-2">
          <Col className="login-btn">
            <Button size="sm" variant="primary" onClick={onLogin}>
              Log in
            </Button>
          </Col>
        </Row>
      </div>
    </>
  );
};
