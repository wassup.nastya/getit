import React, { useCallback, useEffect, useState } from 'react';
import { FormControl, Row, Col, FormLabel, Button } from 'react-bootstrap';
import icon from '../images/calendar.png';
import { Event } from '../data/model';
import DatePicker from 'react-date-picker';
import Select from 'react-select';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { StoreType } from '../core/rootReducer';
import {
  addEventAsync,
  getEventAsync,
  setUser,
  updateEventAsync,
  updateUserAsync,
} from '../data/actions';

import './new-event.css';

const options: { value: string; label: string }[] = [
  { value: 'C', label: 'C' },
  { value: 'Java', label: 'Java' },
  { value: 'AI', label: 'AI' },
  { value: 'Frontend', label: 'Frontend' },
  { value: 'Backend', label: 'Backend' },
  { value: 'JavaScript', label: 'JavaScript' },
  { value: 'Go', label: 'Go' },
  { value: 'ML', label: 'ML' },
  { value: 'Soft Skills', label: 'Soft Skills' },
  { value: 'DevOps', label: 'DevOps' },
];

const defaultEvent: Event = {
  id: '',
  title: '',
  date: new Date(),
  creator: '',
  description: '',
  tags: [],
  users: [],
};

export const EditEvent = () => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const dispatch = useDispatch();

  const [event, setEvent] = useState<Event>(defaultEvent);
  const [tags, setTags] = useState<any | undefined>(undefined);

  const { isLogin, event: originalEvent, user } = useSelector(
    (state: StoreType) => state.data
  );

  if (!isLogin) history.push('/');

  useEffect(() => {
    if (id !== 'new') dispatch(getEventAsync(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (originalEvent && id !== 'new') {
      setEvent(originalEvent);
      setTags(originalEvent?.tags?.map((x) => ({ label: x, value: x })));
    }
  }, [originalEvent, id]);

  const onSave = useCallback(() => {
    if (id === 'new') {
      dispatch(
        addEventAsync({
          event: {
            ...event,
            tags: tags?.map((x) => x?.label),
            creator: user?.id ?? '',
          },
          onResponse: (x) => {
            const newEvents = [...user.events];
            newEvents.push(x);
            dispatch(
              updateUserAsync({
                ...user,
                events: newEvents,
              })
            );
            dispatch(
              setUser({
                ...user,
                events: newEvents,
              })
            );
            history.push('/');
          },
        })
      );
    } else {
      dispatch(
        updateEventAsync({ ...event, tags: tags?.map((x) => x?.label) })
      );
      history.push('/');
    }
  }, [dispatch, event, id, history, tags, user]);

  return (
    <div className="event-container">
      <div className="back">
        <Button
          size="sm"
          variant="primary"
          onClick={() => {
            history.push('/');
          }}
        >
          Back to list
        </Button>
      </div>
      <Row>
        <Col xs={6} className="new-event">
          {`${id !== 'new' ? 'Edit' : 'New'} event`}
        </Col>
      </Row>

      <img src={icon} alt="icon" className="icon" />

      <FormLabel className="title-label">Title</FormLabel>
      <FormControl
        value={event.title}
        onChange={(e) => setEvent({ ...event, title: e.target.value })}
        size="sm"
      />

      <FormLabel className="description-label">Description</FormLabel>
      <textarea className="description-input"
        value={event.description}
        onChange={(e) => setEvent({ ...event, description: e.target.value })}
      />

      <FormLabel className="date-label">Date</FormLabel>
      <div className="form-control1">
        <DatePicker
          value={event.date}
          defaultValue={event.date}
          onChange={(v) =>
            setEvent({ ...event, date: v instanceof Array ? v?.[0] : v })
          }
          dayPlaceholder="dd"
          monthPlaceholder="mm"
          yearPlaceholder="yyyy"
        />
      </div>

      <FormLabel className="tags-label">Tags</FormLabel>
      <div className="form-control1">
        <Select
          value={tags}
          defaultValue={tags}
          onChange={(value) => setTags(value)}
          isMulti
          name="colors"
          className="basic-multi-select"
          classNamePrefix="select"
          options={options}
        />
      </div>

      <div className="save">
        <Button size="sm" variant="primary" onClick={onSave}>
          Save
        </Button>
      </div>
    </div>
  );
};
