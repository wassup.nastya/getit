import firebase from "firebase";

import "firebase/firestore";

firebase.initializeApp({
  apiKey: "AIzaSyDEzibPpnYKrNuKZDbdFHY9oWwlR1tz3Rk",
  authDomain: "fir-c5f24.firebaseapp.com",
  databaseURL: "https://fir-c5f24.firebaseio.com",
  projectId: "fir-c5f24",
  storageBucket: "fir-c5f24.appspot.com",
  messagingSenderId: "787768640178",
  appId: "1:787768640178:web:fb2862401fc0912d743fab",
  measurementId: "G-RW0D4SQGQ1",
});

let db = firebase.firestore();

export default {
  firebase,
  db,
};