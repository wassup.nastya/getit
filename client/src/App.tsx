import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Events } from './component/events';
import { Login } from './component/login';
import { EditEvent } from './component/new-event';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/event/:id" component={EditEvent} />
        <Route exact path="/" component={Events} />
      </Switch>
    </div>
  );
}

export default App;
